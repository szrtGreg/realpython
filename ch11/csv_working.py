import os
import csv

os.chdir('ch11')
path = os.getcwd()

with open(os.path.join(path, 'practise.csv'), 'w',  newline='') as my_file:
    writer = csv.writer(my_file)
    writer.writerow(['FirstName','LastName','Age'])
    writer.writerow(['Prze', 'Gurt','22'])
    writer.writerow(['Jus', 'Okul','12'])



with open(os.path.join(path, 'practise.csv'), 'r') as my_file:
    reader = csv.reader(my_file)
    next(reader)
    for row in reader:
        print(row)

