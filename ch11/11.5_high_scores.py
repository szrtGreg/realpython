import os
import csv
from pathlib import Path

os.chdir('ch11')
path = os.getcwd()

with open(os.path.join(path, 'scores.csv'), 'r', encoding="utf-8") as my_file:
    reader = csv.DictReader(my_file)
    scores = [row for row in reader]

print(scores)

high_scores = {}
for item in scores:
    name = item["name"]
    score = int(item["score"])
    if name not in high_scores:
        high_scores[name] = score
    else:
        if score > high_scores[name]:
            high_scores[name] = score

print(high_scores)
new_file_path = Path(os.path.join(path, 'high.csv'))

with new_file_path.open(mode="w", encoding="utf-8", newline='') as new_file:
    writer=csv.DictWriter(new_file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for name in high_scores:
        row={"name":name, "high_score":high_scores[name]}
        writer.writerow(row)
