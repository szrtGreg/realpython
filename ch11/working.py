import os
import glob
os.chdir('ch11')

#11.1.1
file = open('poem.txt', 'r')
print(file.readlines())
file.close()

print('\n')
file = open('poem.txt', 'r')
for line in file.readlines():
    print(line, end="")
file.close()


#11.1.2
print('\n')
with open('poem.txt', 'r') as input_file:
    for line in input_file.readlines():
        print(line, end="")

#11.1.3
print('\n')
with open('poem.txt', 'r') as source, open('hi.txt','w') as dest:
    for line in source.readlines():
        dest.write(line)


#11.1.4
print('\n')
with open('poem.txt', 'a') as input_file:
    input_file.writelines('\nTekst')



#11.2
print('\n#11.2')
input_file = open(r'c:\newWork\realpython\ch11\poem.txt')
input_file = open('c:/newWork/realpython/ch11/poem.txt')

path = r'c:\newWork\realpython\ch11'
for file_name in os.listdir(path):
    print(file_name)
    full_path = os.path.join(path, file_name)
    print(full_path)
    new_file_name = full_path[:-4] + '_backup.txt'
    print(new_file_name)

print('\n#11.2')
path = r'c:\newWork\realpython\ch11'
possible_files = os.path.join(path, '*.txt')
for file_name in glob.glob(possible_files):
    print(file_name)



print('\n#11.2')
path = r'c:\newWork\realpython\ch11'

files_and_folders = os.listdir(path)
for folder_name in files_and_folders:
    full_path = os.path.join(path, folder_name)
    if os.path.isdir(full_path):
        print(full_path + 'folder')


print('\n#11.2')
path = r'c:\newWork\realpython\ch11'

for current_folder, subfolders, file_names in os.walk(path):
    for file_name in file_names:
        print(os.path.join(current_folder, file_name))
