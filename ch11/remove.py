import os


path = os.getcwd()
for current_folder, subfolders, file_names in os.walk(path):
    for file_name in file_names:
        fullpath = os.path.join(current_folder, file_name)
        if file_name.endswith('delete.txt') and os.path.getsize(fullpath) < 2000:
            os.remove(fullpath)


