class Animal:

    def __init__(self, name, age):
        self.name = name
        self.age = age
    
    def __str__(self):
        return f"{self.name} is {self.age}"

    def walk(self):
        return f"{self.name} walk"

    def sleep(self):
        return f"{self.name} sleep"

    def talk(self, sound="heelo"):
        return f"{self.name} says {sound}"


class Dog(Animal):
    def walk(self):
        return f"{self.name} is running"


class Pig(Animal):
    pass

class Cow(Animal):
    pass

buddy = Dog("buddy", 3)
miles = Pig("miles", 2)
jack = Cow("jack", 8)

print(buddy)
print(buddy.walk())
print(miles.walk())
print(miles.talk())
print(jack.talk("chrum"))
