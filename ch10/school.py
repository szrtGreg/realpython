

class Person:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    
class Student(Person):
    def __init__(self, class_num):
        self.class_num = class_num


class Teacher(Person):
    def __init__(self, classes):
        self.classes = classes


    