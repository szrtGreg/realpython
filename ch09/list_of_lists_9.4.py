from statistics import median

universities = [
    ['California Institute of Technology', 5, 10],
    ['Harvard', 10, 11],
    ['Massachusetts Institute of Technology', 4, 12],
    ['Princeton', 15, 13],
    ['Rice', 12, 14],
]


def enrollment_stats(list_of_uni):
    total_students = []
    total_fees = []
    for uni in list_of_uni:
        total_students.append(uni[1])
        total_fees.append(uni[2])
    
    return total_students, total_fees


def mean(values):
    return sum(values) / len(values)

result = enrollment_stats(universities)


print(f'Students: {sum(result[0])} Fees: {sum(result[1])}')
print(f'Mean: {mean(result[0])}')
print(f'Mediana {median(result[0])}')
