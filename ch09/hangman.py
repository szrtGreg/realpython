# HangMan
import os

secret_word = 'grzegorz'
guessed_letters = ''
failure_count = 6


def draw_hang(failure_count):
    os.system('cls')
    if failure_count == 5:
        
        print(''' 
        |
        |
        |
        |
        |
        |
        |
        ''')

    if failure_count == 4:       
        print(''' 
        |----
        |
        |
        |
        |
        |
        ''')

    if failure_count == 3:       
        print(''' 
        |----
        |    |
        |    O
        |    
        |    
        |   
        ''')

    if failure_count == 2:       
        print(''' 
        |----
        |    |
        |    O
        |   /|\  
        |    
        |   
        ''')


    if failure_count == 1:       
        print(''' 
        |----
        |    |
        |    O
        |   /|\  
        |    |
        |   
        ''')

    if failure_count == 0:       
        print(''' 
        |----
        |    |
        |    O
        |   /|\  
        |    |
        |   / \\
        ''')






while failure_count > 0:
    guess = input('Enter a letter: ')
    if guess in secret_word:
        print(f'Correct there is one or more {guess} in the word')
    else:
        failure_count -= 1
        draw_hang(failure_count)
        print(f'Incorect no {guess} letters in secret word.')
    
    guessed_letters = guessed_letters + guess
    wrong_letter_count = 0

    for letter in secret_word:
        if letter in guessed_letters:
            print(f'{letter}', end='')
        else:
            print(' _ ', end='')
            wrong_letter_count += 1
    print('')
    
    if wrong_letter_count == 0:
        print(f'Congrats, secret word was: {secret_word}')
        break

else:
    print('Sorry, you didnt guess')