import random

capitals_dict = {
'Alabama': 'Montgomery',
'Alaska': 'Juneau',
'Arizona': 'Phoenix',
'Arkansas': 'Little Rock',
'California': 'Sacramento',
'Colorado': 'Denver',
'Connecticut': 'Hartford',
'Delaware': 'Dover',
'Florida': 'Tallahassee',
'Georgia': 'Atlanta',
}

states_list = list(capitals_dict)
guess = False

while not guess:
    state = random.choice(states_list)
    answer = input(f'What is capital of {state}? ')
    if answer.lower() == capitals_dict[state].lower():
        guess = True
        print('Correct')

print('Bye')
