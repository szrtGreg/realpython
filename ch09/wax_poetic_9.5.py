import random

nouns = ["follsil", "horse", "jusge", "chef"]
verbs = ["kicks", "jingles", "slurps", "explodes"]
adj = ["furry", "balding", "fragrant", "soup"]
prepositions = ["agains", "after", "into", "upon"]
adverbs = ["curiously", "extravagantly", "sensuously", "dootly"]


def make_poem():
    
    n1 = random.choice(nouns)
    n2 = random.choice(nouns)
    n3 = random.choice(nouns)

    while n1 == n2:
        n2 = random.choice(nouns)
    while n3 == n2 or n3 == n1:
        n3 = random.choice(nouns)

    v1 = random.choice(verbs)
    v2 = random.choice(verbs)
    v3 = random.choice(verbs)

    while v1 == v2:
        v2 = random.choice(verbs)
    while v3 == v2 or v3 == v1:
        v3 = random.choice(verbs)



    ad1 = random.choice(adj)
    ad2 = random.choice(adj)
    ad3 = random.choice(adj)

    while ad1 == ad2:
        ad2 = random.choice(adj)
    while ad3 == ad2 or ad3 == ad1:
        ad3 = random.choice(adj)

    poem = (
        f"{n1} {n2} {v1}\n\n"
        f"{v2} {v3} {ad1} {ad2} {n1} the {ad3} {n1}\n"
    )

    return poem


poem = make_poem()
print(poem)