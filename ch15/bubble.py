def bubble_sort(current_list):
    num_of_elements = len(current_list)
    temp = ''

    for x in range(num_of_elements):
        for y in range(1, num_of_elements):
            if current_list[y-1] <= current_list[y]:
                continue
            temp = current_list[y-1]
            current_list[y-1] = current_list[y]
            current_list[y] = temp
        
    print(current_list)

        
lista1 = [1, 3, 4, 67, 5]
lista2 = []
lista3 = [1, 1, 1, 1]
lista4 = [14, 32, 1, 5]

for current_list in (lista1, lista2, lista3, lista4):
    bubble_sort(current_list)
