import time
import random

class Board:
    def __init__(self):
        self.board = self.make_new_board()
        self.turn = 'x'

    
    def change_turns(self):
        if self.turn == 'x':
            self.turn = 'o'
        else:
            self.turn = 'x'


    def make_new_board(self):
        board = [['.' for _ in range(8)] for _ in range(8)]

        #Crate board
        for r in range(8):
            for c in range(8):
                if (r + c) % 2 == 0:
                    board[r][c] = '_'
        for r in range(3):
            for c in range(8):
                if (r + c) % 2 == 0:
                    board[r][c] = 'o'

        for r in range(5,8):
            for c in range(8):
                if (r + c) % 2 == 0:
                    board[r][c] = 'x'
        return board


    def delta_x(self, old_x, old_y, new_x, new_y):
        return new_x == max(0, old_x-1) or new_x == min(7, old_x+1)

    def delta_y(self, old_x, old_y, new_x, new_y):
        return new_y == max(0, old_y-1) or new_y == min(7, old_y+1)

    def backwards(self, old_x, old_y, new_x, new_y):
        return new_y < old_y

    def move(self, old_x, old_y, new_x, new_y):
        self.board[old_y][old_x] = '_'
        self.board[new_y][new_x] = self.turn
        self.change_turns()


    def is_within_bound(self, x, y):
        return (0 <= x and x < 8 and 0 <= y and y < 8)

    def set(self, row, col, value):
        self.board[row][col] = value


    def possible_computer_moves(self):
        valid_moves = []
        for r in range(8):
            for c in range(8):
                if self.board[r][c] == 'o':
                    new_y = r + 1
                    new_x = c + 1
                    if self.board[max(0,new_y)][min(7,new_x)] == '_':
                        valid_moves.append((r,c,new_y, new_x))
                    new_x = c - 1
                    #tu moze cos byc nie tak
                    if self.board[max(0,new_y)][max(0,new_x)] == '_':
                        valid_moves.append((r,c,new_y, new_x))
        return valid_moves


    def possible_capture(self):
        valid_moves = []
        #zrobic z tego metode
        if self.turn == 'x':
            oponent_pawn = 'o'
        else:
            oponent_pawn = 'x'
        for r in range(8):
            for c in range(8):
                if self.board[r][c] == self.turn:
                    #dol prawy
                    new_y = min(7, r + 2)
                    new_x = min(7, c + 2)
                    oponent_y = min(7, r + 1)
                    oponent_x = min(7, c + 1)
                    if self.board[new_y][new_x] == '_' and self.board[oponent_y][oponent_x] == oponent_pawn:
                        valid_moves.append((r,c, new_y, new_x , oponent_y,oponent_x))
                    #dol lewy 
                    new_y = min(7, r + 2)
                    new_x = max(0, c - 2)
                    oponent_y = min(7, r + 1)
                    oponent_x = max(0, c - 1)
                    if self.board[new_y][new_x] == '_' and self.board[oponent_y][oponent_x] == oponent_pawn:
                        valid_moves.append((r,c, new_y, new_x, oponent_y,oponent_x))
                    #gora prawy
                    new_y = max(0, r - 2)
                    new_x = min(7, c + 2)
                    oponent_y = max(0, r - 1)
                    oponent_x = min(7, c + 1)
                    if self.board[new_y][new_x] == '_' and self.board[oponent_y][oponent_x] == oponent_pawn:
                        valid_moves.append((r,c, new_y, new_x ,oponent_y,oponent_x))
                    #gora lewy
                    new_y = max(0, r - 2)
                    new_x = max(0, c - 2)
                    oponent_y = max(0, r - 1)
                    oponent_x = max(0, c - 1)
                    if self.board[new_y][new_x] == '_' and self.board[oponent_y][oponent_x] == oponent_pawn:
                        valid_moves.append((r,c, new_y, new_x, oponent_y,oponent_x))
                                        
        return valid_moves


    def capture(self, data_to_capture):
        dtc = data_to_capture
        self.move(dtc[1], dtc[0], dtc[3], dtc[2])
        self.board[dtc[4]][dtc[5]] = '_'

    def count_o(self):
        amount = 0
        for r in range(8):
            for c in range(8):
                if self.board[r][c] == 'o':
                    amount +=1
        return amount

    def count_x(self):
        amount = 0
        for r in range(8):
            for c in range(8):
                if self.board[r][c] == 'x':
                    amount +=1
        return amount


    def show(self):
        print(' ')
        print('  0 1 2 3 4 5 6 7')
        print('=================')
        colum_number = 0
        for row in self.board:
            print(colum_number, *row)
            colum_number += 1


b = Board()
b.show()
while not b.count_o == 0 or not b.count_x == 0:
    print(f'Moje Bicia:{b.possible_capture()} ')
    user_input = input('Podaj old_x old_y new_x new_y: ').split()
    old_x, old_y, new_x, new_y = int(user_input[0]),\
    int(user_input[1]), int(user_input[2]), int(user_input[3])
    
    possible_capture = b.possible_capture()
    if possible_capture:
        for i in possible_capture:
            if (i[2], i[3]) == (new_y, new_x):
                b.capture(i)
                b.show()

                time.sleep(2)
                print(f'Wroga Bicia:{b.possible_capture()} ')
                possible_capture = b.possible_capture()
                if possible_capture:
                    b.capture(possible_capture[0])
                    b.show()
                else:                   
                    old_y, old_x, new_y, new_x = random.choice(b.possible_computer_moves())
                    b.move(old_x, old_y, new_x, new_y)
                    b.show()
            else:
                continue
    else:
        if not b.is_within_bound(new_x, new_y):
            print('Out of bounds')
            continue

        if not b.is_within_bound(old_x, old_y):
            print('Out of bounds')
            continue
        #correct_pawn() not
        if b.board[old_y][old_x] != 'x':
            print('Not this pawn')
            continue

        #is_valid_field() not
        if b.board[new_y][new_x] != '_':
            print('Invalid filed')
            continue
        
        #go_streight
        if not b.backwards(old_x, old_y, new_x, new_y):
            print('No backwards')
            continue
        #is_valid_delta_x
        if not b.delta_x(old_x, old_y, new_x, new_y):
            print('Delta to high')
            continue

        if not b.delta_y(old_x, old_y, new_x, new_y):
            print('Delta y to high')
            continue
        
        b.move(old_x, old_y, new_x, new_y)
 
        b.show()

        time.sleep(2)
        
        #print(f'Bicia wroga: {b.possible_capture()}')
        #print(b.possible_computer_moves())
        print(f'Wroga Bicia:{b.possible_capture()} ')
        possible_capture = b.possible_capture()
        if possible_capture:
            b.capture(possible_capture[0])
            b.show()
        else:  
            old_y, old_x, new_y, new_x = random.choice(b.possible_computer_moves())
            b.move(old_x, old_y, new_x, new_y)
            b.show()

