import sqlite3
connection = sqlite3.connect('ch14/test_db.db')
cursor = connection.cursor()

query = "SELECT * FROM People;"
people = cursor.execute(query).fetchall()
print(people)
connection.close()


values = (
    ('Ron', 'Obvouius', 12),
    ('Ron2', 'Obvouius2', 423),
    ('Ron3', 'Obvouius3', 33)
)

with sqlite3.connect('ch14/test_db.db') as connection:
    cursor = connection.cursor()
    cursor.executescript(
        """
        DROP TABLE IF EXISTS People;

        CREATE TABLE People(
            FirstName TEXT,
            LastName TEXT,
            Age INT
        );

        """
    )

    cursor.executemany("INSERT INTO People VALUES(?,?,?);", values)

    cursor.execute("SELECT FirstName, LastName FROM People WHERE Age > 30;")

    for row in cursor.fetchall():
        print(row)
