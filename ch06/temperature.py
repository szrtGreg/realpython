# 6.3 temperature.py

def convert_cel_to_far(cel):
    far = cel * 9/5 + 32
    return far

def convert_far_to_cel(far):
    cel = (far - 32) * 5/9
    return cel

far = float(input("Enter a temperature in degrees F "))
cel = convert_far_to_cel(far)
print(f'{far} degrees F = {cel:.2f} degress C')

cel = float(input("Enter a temperature in degrees C "))
far = convert_cel_to_far(cel)
print(f'{cel} degrees C = {far:.2f} degress F')