import random

heads_tally = 0
tails_tally = 0

def coin_flip_count():
    flip_result = random.randint(0, 1)
    flip_count = 1

    while flip_result == random.randint(0, 1):
        flip_count = flip_count + 1
    
    flip_count = flip_count + 1
    return flip_count


total = 0
num_trails = 3
for trail in range(num_trails):
    total = total + coin_flip_count()

total = total / num_trails
