
import tkinter as tk

def celsius_to_far(celsius, digits=3):
    far = float(celsius) * (9 / 5 ) + 32
    rounded = round(far, digits)
    return rounded

def convert():
    celsius = celsius_entry.get()

    try:
        result = str(celsius_to_far(celsius))
    except ValueError:
        result = "Invalid"

    result_display.config(text=result)



window = tk.Tk()
window.title('Temperature convertrer')

window.resizable(width=False, height=False)

conversion_frame = tk.Frame(master=window)
conversion_frame.pack(side=tk.LEFT, padx=5, pady=5)

button_frame = tk.Frame(master=window)
button_frame.pack(side=tk.LEFT,  padx=5, pady=5)

celsius_entry_label = tk.Label(master=conversion_frame)
celsius_entry_label['text'] = 'Degrees (Celsius):'
celsius_entry_label.grid(row=0, column=0)

celsius_entry = tk.Entry(master=conversion_frame)
celsius_entry.grid(row=0, column=1)

result_label = tk.Label(master=conversion_frame)
result_label['text'] = 'Degrees Farrenhait:'
result_label.grid(row=1, column=0)

result_display = tk.Label(master=conversion_frame)
result_display.grid(row=1, column=1)

convert_button = tk.Button(master=button_frame)
convert_button['text'] = 'Convert'
convert_button['command'] = convert
convert_button.pack()

window.mainloop()