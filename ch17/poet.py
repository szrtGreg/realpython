import tkinter as tk
import random
from tkinter import filedialog


def make_poem():
    
    nouns = nonuns_entry.get()
    nouns = list(nouns.split())
    if len(nouns) <= 3:
        return

    verbs = verbs_entry.get()
    verbs = list(verbs.split())

    if len(verbs) <= 3:
        return

    adj = adj_entry.get()
    adj = list(adj.split())

    if len(adj) <= 3:
        return

    n1 = random.choice(nouns)
    n2 = random.choice(nouns)
    n3 = random.choice(nouns)

    while n1 == n2:
        n2 = random.choice(nouns)
    while n3 == n2 or n3 == n1:
        n3 = random.choice(nouns)

    v1 = random.choice(verbs)
    v2 = random.choice(verbs)
    v3 = random.choice(verbs)

    while v1 == v2:
        v2 = random.choice(verbs)
    while v3 == v2 or v3 == v1:
        v3 = random.choice(verbs)



    ad1 = random.choice(adj)
    ad2 = random.choice(adj)
    ad3 = random.choice(adj)

    while ad1 == ad2:
        ad2 = random.choice(adj)
    while ad3 == ad2 or ad3 == ad1:
        ad3 = random.choice(adj)



    poem = (
        f"{n1} {n2} {v1}\n\n"
        f"{v2} {v3} {ad1} {ad2} {n1} the {ad3} {n1}\n"
    )

    poem_label['text'] = poem



def save_file_as():
    filepath = filedialog.askopenfilename(
        defaultextension='.txt',
        filetypes=[('Text File', '*.txt')]
    )

    if not filepath:
        return

    with open(filepath, 'w') as output_file:
        text = poem_label['text']
        output_file.write(text)

    window.title(f'Text Editor - {filepath}')



window = tk.Tk()
window.title('Poet')
window.geometry('400x400')


top_frame= tk.Frame(master=window)
top_frame.pack(fill=tk.X)

title_label = tk.Label(master=top_frame)
title_label ['text'] = 'Enter your words'
title_label.pack()

##############NOUN
middle_frame1 = tk.Frame(master=window)
middle_frame1['relief'] = tk.GROOVE
middle_frame1.pack(fill=tk.X, padx=10)

nonuns_label = tk.Label(master=middle_frame1)
nonuns_label['text'] = 'Nouns      '
nonuns_label.pack(side=tk.LEFT, fill=tk.X)

nonuns_entry = tk.Entry(master=middle_frame1)
nonuns_entry.pack(side=tk.LEFT, fill=tk.X, expand=True, padx=5)

################VERB
middle_frame2 = tk.Frame(master=window)
middle_frame2['relief'] = tk.GROOVE
middle_frame2.pack(fill=tk.X, padx=10)

verbs_label = tk.Label(master=middle_frame2)
verbs_label['text'] = 'Verbs        '
verbs_label.pack(side=tk.LEFT, fill=tk.X)

verbs_entry = tk.Entry(master=middle_frame2)
verbs_entry.pack(side=tk.LEFT, fill=tk.X, expand=True, padx=5)

################ADJ
middle_frame3 = tk.Frame(master=window)
middle_frame3['relief'] = tk.GROOVE
middle_frame3.pack(fill=tk.X, padx=10)

adj_label = tk.Label(master=middle_frame3)
adj_label['text'] = 'Adjectives'
adj_label.pack(side=tk.LEFT, fill=tk.X)

adj_entry = tk.Entry(master=middle_frame3)
adj_entry.pack(side=tk.LEFT, fill=tk.X, expand=True, padx=5)

################Generate

generate_frame= tk.Frame(master=window)
generate_frame.pack(fill=tk.X)

genrate_button = tk.Button(master=generate_frame)
genrate_button['text'] = 'Generate'
genrate_button['command'] = make_poem
genrate_button.pack()


################Poem Result
poem_frame= tk.Frame(master=window)
poem_frame.pack(fill=tk.BOTH, expand=True, padx=10, pady=10)
poem_frame['relief'] = tk.GROOVE
poem_frame['borderwidth'] = 2


poem_label = tk.Label(master=poem_frame)
poem_label.pack()

save_as_button = tk.Button(master=poem_frame)
save_as_button['text'] = 'Save to file'
save_as_button['command'] = save_file_as
save_as_button.pack()

window.mainloop()