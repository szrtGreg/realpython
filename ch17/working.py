""" import easygui as gui
from PyPDF2 import PdfFileReader, PdfFileWriter

file_type = "*.pdf"

input_path = gui.fileopenbox(title="Select a file", default="*.pdf")

if input_path is None:
    exit()


choices = ("90", "180", "270")
message = "Rotate the PDF clockwise by how many degrees?"
degrees = gui.buttonbox(message, "Choose rotation...", choices)
degrees = int(degrees)

save_title = "Save the rotated PDF as..."
output_path = gui.filesavebox(title=save_title, default=file_type)

warn_title = "Warning!"
warn_message = "Cannot overwrite original file!"

while input_path == output_path:
    gui.msgbox(warn_message, warn_title)
    output_path = gui.filesavebox(title=save_title, default=file_type)

if output_path is None:
    exit()


input_file = PdfFileReader(input_path)
output_pdf = PdfFileWriter()

for page in input_file.pages:
    page = page.rotateClockwise(degrees)
    output_pdf.addPage(page)

with open(output_path, "wb") as output_file:
    output_pdf.write(output_file)
 """

""" import tkinter as tk
window = tk.Tk()
greeting = tk.Label(text="Hello, Tkinter")
greeting.pack()

button = tk.Button()
button['width'] = 25
button['height'] = 5
button['bg'] = 'blue'
button['fg'] = 'yellow'
button['text'] = 'Click me'
button.pack()


entry = tk.Entry()
entry.insert(0, 'Hello')
text_entered = entry.get()
entry.pack()


text_box = tk.Text()
text_box.insert('1.0', 'Hello')
text_box.pack()

window.mainloop()
 """

""" import tkinter as tk

window = tk.Tk()

frame_a = tk.Frame()
label_a = tk.Label(master=frame_a, text="I am A")
label_a.pack(side="top")

frame_b = tk.Frame()
label_b = tk.Label(master=frame_b, text="I am B")
label_b.pack()

frame_b.pack()
frame_a.pack()


window.mainloop() """


""" import tkinter as tk

border_effects = {
    "flat": tk.FLAT,
    "sunken": tk.SUNKEN,
    "raised": tk.RAISED,
    "groove": tk.GROOVE,
    "ridge": tk.RIDGE,
}
window = tk.Tk()

for relief_name in border_effects:
    frame = tk.Frame(master=window)
    frame["relief"] = border_effects[relief_name]
    frame["borderwidth"] = 5 
    frame.pack(side=tk.LEFT)

    label = tk.Label(master=frame)
    label["text"] = relief_name
    label.pack()

window.mainloop() """



""" import tkinter as tk

window = tk.Tk()

frame1 = tk.Frame(master=window, width=100, height=100, background="red")
frame1.pack(fill=tk.Y)

frame2 = tk.Frame(master=window, width=50, height=100, background="yellow")
frame2.pack(fill=tk.Y)

window.mainloop() """


""" import tkinter as tk

window = tk.Tk()
frame1 = tk.Frame(master=window, width=150, height=150, )
frame1.pack()

label1 = tk.Label(master=frame1, text="I am 0,0", background="red")
label1.place(x=0, y=0)

label2 = tk.Label(master=frame1, text="I am 0,0", background="yellow")
label2.place(x=75, y=75)


window.mainloop() """


""" import tkinter as tk
window = tk.Tk()

for i in range(0, 3):
    window.columnconfigure(i, weight=1, minsize=75)
    window.rowconfigure(i, weight=1, minsize=50)
    for j in range(0, 3):
        frame = tk.Frame(master=window)
        frame['relief'] = tk.RAISED
        frame['borderwidth'] = 4
        frame.grid(row=i, column=j, padx=5, pady=5, ipadx=5, ipady=5)

        label = tk.Label(master=frame)
        label["text"] = f"Row {i}\nColumn {j}"
        label.pack(padx=5, pady=5)



window.mainloop()
 """
 
""" import tkinter as tk
window = tk.Tk()
label1 = tk.Label()
label1["text"] = "A short label"
label1.grid(row=1, column=0, sticky=tk.E)


label2 = tk.Label()
label2["text"] = "A much longer label"
label2.grid(row=22, column=0, sticky=tk.E)


window.mainloop() """

""" import tkinter as tk

def increase():
    value = int(value_label['text'])
    value_label['text'] = str(value + 1)


def decrease():
    value = int(value_label['text'])
    value_label['text'] = str(value - 1)

window = tk.Tk()

decrease_button = tk.Button(master=window)
decrease_button['text'] = '-'
decrease_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

value_label = tk.Label(master=window)
value_label['text'] = '0'
value_label.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

increase_button = tk.Button(master=window)
increase_button['text'] = '+'
increase_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

increase_button['command'] = increase
decrease_button['command'] = decrease

window.mainloop()
 """


""" 
import tkinter as tk

def celsius_to_far(celsius, digits=3):
    far = float(celsius) * (9 / 5 ) + 32
    rounded = round(far, digits)
    return rounded

def convert():
    celsius = celsius_entry.get()

    try:
        result = str(celsius_to_far(celsius))
    except ValueError:
        result = "Invalid"

    result_display.config(text=result)



window = tk.Tk()
window.title('Temperature convertrer')

window.resizable(width=False, height=False)

conversion_frame = tk.Frame(master=window)
conversion_frame.pack(side=tk.LEFT, padx=5, pady=5)

button_frame = tk.Frame(master=window)
button_frame.pack(side=tk.LEFT,  padx=5, pady=5)

celsius_entry_label = tk.Label(master=conversion_frame)
celsius_entry_label['text'] = 'Degrees (Celsius):'
celsius_entry_label.grid(row=0, column=0)

celsius_entry = tk.Entry(master=conversion_frame)
celsius_entry.grid(row=0, column=1)

result_label = tk.Label(master=conversion_frame)
result_label['text'] = 'Degrees Farrenhait:'
result_label.grid(row=1, column=0)

result_display = tk.Label(master=conversion_frame)
result_display.grid(row=1, column=1)

convert_button = tk.Button(master=button_frame)
convert_button['text'] = 'Convert'
convert_button['command'] = convert
convert_button.pack()

window.mainloop() """